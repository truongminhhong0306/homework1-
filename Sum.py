def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum

print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))

# O day ta co lenh dieu kien neu sum trong khoang tu 15 den 20 thi ham se tra ve gia tri mac dinh la 20; neu khong thi tra ve gia tri cua x + y
# Vi vay ta co the thay lenh dau tien se cho ta gia tri 20
# Con hai lenh o duoi se cho ta cac gia tri lan luot la 12 va 22
